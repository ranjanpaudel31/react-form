import React from 'react';

import Button from './components/Button';
import TextField from './components/TextField';

class ReactForm extends React.Component {
  render() {
    return (
      <div className="main-container">
        <TextField
          id='text-field-1'
          name='textField1'
          fullWidth
          placeholder="Enter some text"
          size="small"
          initialValue="Real time"
          disabled
        />
        <TextField
          id='text-field-2'
          name='textField2'
          cols={100}
          placeholder="Enter some text"
          size="small"
          initialValue="Real time"
        />
        <TextField
          id='text-field-3'
          multiline
          fullWidth
          rows="4"
          name='textField3'
          placeholder="Enter some text"
          size="small"
          initialValue="Real time"
        />
        This is button
      </div>
    );
  }
}

export default ReactForm;
