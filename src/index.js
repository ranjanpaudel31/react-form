import React from 'react';
import ReactDOM from 'react-dom';

import ReactForm from './ReactForm';

import './styles/index.css'

ReactDOM.render(<ReactForm />, document.getElementById('react-form-root'));
