import React, { useState } from 'react';

import '../styles/text-field.css'

/**
 * @param {Object} props React props
 * @param {string?} props.id Unique ID of the text-field - optinal.
 * @param {string?} props.name Name of text-field.
 * @param {string?} props.placeholder Placeholder for the text-field.
 * @param {string?} props.initialValue Initial value to put on the field
 * @param {('small' | 'medium' | 'large')} props.size TextField size. 
 * @param {boolean?} props.fullWidth 
 * @param {boolean?} props.multiline Flag to choose multiline feature.
 * @param {number?} props.rows Number of character rows (only for multiline field).
 * @param {number?} props.cols Number of character columns (only for multiline field)
 * @param {boolean} props.disabled TextField disable flag.
 * @param {function} props.onChange onChange event handler.
 * @param {CSSStyleDeclaration} props.style To add custom inline styling.
 * 
 * @returns {HTMLInputElement} input type 'text'
 */
function TextField(props) {
  const { size = 'medium', multiline = false, initialValue = '', fullWidth = false } = props;

  const fieldClassName = ['text-field', multiline ? '' : size, fullWidth ? 'fullWidth' : ''].join(' ').trimEnd();
  const propsCopy = { ...props };

  delete propsCopy.multiline;
  delete propsCopy.fullWidth;
  delete propsCopy.initialValue;

  const [value, setValue] = useState(initialValue);
  const onChange = event => {
    setValue(event.target.value);
  }

  if (!multiline) {
    return (
      <input
        {...propsCopy}
        type="text"
        value={value}
        className={fieldClassName}
        onChange={onChange}
      />
    )
  }

  return (
    <textarea
      {...propsCopy}
      value={value}
      className={fieldClassName}
      onChange={onChange}
    />
  )
}

export default TextField;
