import React from 'react';

import '../styles/button.css'

/**
 * Customized button
 * 
 * @param {Object} props React props
 * @param {string?} props.id Unique ID of the button - optinal.
 * @param {('default' | 'blue' | 'red' | 'green' | 'orange')} props.color Button background color. Text color is set accordingly.
 * @param {('small' | 'medium' | 'large')} props.size Button size. 
 * @param {boolean} props.disabled Button disable flag.
 * @param {function} props.onClick onClick event handler.
 * @param {CSSStyleDeclaration} props.style To add custom inline styling.
 * 
 * @returns {HTMLButtonElement} button
 * 
 * @example
 * <Button id="click-me" color="red" size="large" onClick={handleClick}>
 *   Click me
 * <Button>
 */
function Button(props) {
  const { color = 'default', size = 'medium', disabled = false, style = {} } = props;
  let btnClassName = `btn btn-${color} btn-${size}`;
  let overlayClassName = `btn-overlay`;

  if (disabled) {
    btnClassName += ' btn-disabled';
    overlayClassName += ' btn-overlay-disabled';
  }

  return (
    <button
      {...props}
      className={btnClassName}
      onClick={disabled ? () => { } : props.onClick}
      disabled={disabled}
      style={{ ...style }}
    >
      <span className={overlayClassName}>
      </span>
      <span>
        {props.children}
      </span>
    </button>
  );
}

export default Button;
